# log()
#
# Log strings to the terminal with an appropriate timestamp.
#
# @param $@ The string to be printed.
function log {
  echo -e "\033[1;33m[$(date +"%s.%3N")]\033[0m $@"
}

# err()
#
# Log an error string to the terminal with an appropriate timestamp, and exit
# with failure.
#
# @param $@ The string to be printed.
function err {
  echo -e "\033[0;31m[$(date +"%s.%3N")]\033[0m $@"
  exit 1
}

# Remove old files
log "Removing old data..."
rm *.jpg
log "Removed old data."

# Compile the latest
log "Compiling..."
gcc test.c -Ofast -o test || err "Failed to compile."
log "Finished compiling."

# Run the tests
log "Running..."
./test || "Error when running."
log "Finished running."

# Scale JPGs for easier viewing
log "Scaling images..."
for image in *.jpg; do
  log "  Scaling '$image'..."
  convert $image -scale 1024 $image || err "Could not scale image."
done
log "Finished scaling images."
