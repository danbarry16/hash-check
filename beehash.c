/**
 * beehash.c
 *
 * Hash tool.
 *
 * Compile with: gcc beehash.c -Ofast -o beesum
 **/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define HASH_SIZE 4
#define BUFF_SIZE 1024

/**
 * bee_hash()
 *
 * Hash the given data.
 *
 * @param s The string to be hashed.
 * @param k The length of the string,
 * @param d The allocated destination of the hash of at least length n.
 * @param n The length of the target hash.
 * @return The hashed data.
 **/
char* bee_hash(char* s, int k, char* d, int n){
  memset(d, 0b10101010, n);
  for(int x = 0; x < k;)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x++ % n] ^= *s++);
  for(int x = 3; ++x < n - 1;)
    *(int16_t*)(d + x) = (*(int16_t*)(d + x) << 3) ^ *(int16_t*)(d + x) +
                         (*(int16_t*)(d + (x % 4)) ^ *(int16_t*)(d + x - 1));
  return d;
}

/**
 * main()
 *
 * Main entry point into the program.
 *
 * @param argc The number of command line arguments.
 * @param argv The command line arguments.
 * @return The exit status of the program, 0 on success, otherwise error.
 **/
int main(int argc, char** argv){
  /* Setup default arguments */
  int hashSize = HASH_SIZE;
  char* filename = NULL;
  bool readstdin = false;
  /* Loop over arguments */
  /* NOTE: We skip the first argument on Unix systems.*/
  for(int x = 1; x < argc; x++){
    /* Check we have an argument */
    if(argv[x][0] == '-'){
      /* No arguments */
      switch(argv[x][1]){
        case '\0' :
          readstdin = true;
          break;
        case 'h' :
          fprintf(stderr,
            "Usage: beesum [OPTION]... [FILE]...\n"
            "  OPTIONs\n"
            "    -   Read standard input\n"
            "    -h  Display this help\n"
            "    -n  Set the number of bytes for hash\n"
            "          <INT> Hash size, default %i bytes, %i bits\n"
            "    -v  Display the program version\n",
            HASH_SIZE, HASH_SIZE * 8
          );
          return 0;
        case 'v' :
          fprintf(stderr, "beesum 0.1.0\n");
          return 0;
      }
      /* One argument */
      if(x + 1 < argc){
        switch(argv[x][1]){
          case 'n' :
            hashSize = atoi(argv[++x]);
            if(hashSize < HASH_SIZE){
              fprintf(stderr, "Minimum hash size is %i, got %i\n", HASH_SIZE, hashSize);
              return 1;
            }
            break;
        }
      }
    }else{
      if(!readstdin && filename == NULL){
        filename = argv[x];
      }else{
        fprintf(stderr, "Unknown argument '%s'\n", argv[x]);
        return 1;
      }
    }
  }
  /* Decide on action */
  readstdin = readstdin || filename == NULL;
  /* Setup shared buffers */
  int l = BUFF_SIZE;
  char* buff = (char*)malloc(l);
  int k = 0;
  int n = 0;
  /* Read from file */
  if(!readstdin){
    FILE* f = fopen(filename, "rb");
    if(f < 0){
      fprintf(stderr, "Could not read from file '%s'\n", filename);
      return 1;
    }
    while((n = fread(buff + k, 1, BUFF_SIZE, f)) > 0){
      k += n;
      if(l - k < BUFF_SIZE){
        l += BUFF_SIZE;
        buff = realloc(buff, l);
      }
    }
    fclose(f);
  /* Read from stdin */
  }else{
    while((n = read(STDIN_FILENO, buff + k, BUFF_SIZE)) > 0){
      k += n;
      if(l - k < BUFF_SIZE){
        l += BUFF_SIZE;
        buff = realloc(buff, l);
      }
    }
  }
  /* Check for minimum input */
  if(k <= 4) return 0;
  /* Perform the hash */
  char* hash = (char*)malloc(hashSize);
  char* r = bee_hash(hash, k, buff, hashSize);
  /* Print the hash */
  for(int x = 0; x < hashSize; x++) fprintf(stdout, "%02hhX", (unsigned char)r[x]);
  fprintf(stdout, "\n");
  /* Free resources */
  free(hash);
  free(buff);
  return 0;
}
