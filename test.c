#include <assert.h>
#include <float.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "lib/stb_image_write.h"

/**
 * test.c
 *
 * Perform some hashing tests to compare speed and hash effectiveness.
 *
 * Compile with: gcc test.c -Ofast -o test
 **/

/* -- Helper functions -- */

void print_arr(unsigned char* s, int n){
  while(--n >= 0){
    fprintf(stderr, "%02x ", *s);
    ++s;
  }
  fprintf(stderr, "\n");
}

/* Source: https://stackoverflow.com/a/776523/2847743 */
static inline uint32_t rotl32 (uint32_t n, unsigned int c)
{
  const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);  // assumes width is a power of 2.

  // assert ( (c<=mask) &&"rotate by type width or more");
  c &= mask;
  return (n<<c) | (n>>( (-c)&mask ));
}

static inline uint16_t rotl16 (uint16_t n, unsigned int c)
{
  const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);  // assumes width is a power of 2.

  // assert ( (c<=mask) &&"rotate by type width or more");
  c &= mask;
  return (n<<c) | (n>>( (-c)&mask ));
}

/* Source: https://stackoverflow.com/a/776523/2847743 */
static inline uint32_t rotr32 (uint32_t n, unsigned int c)
{
  const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);

  // assert ( (c<=mask) &&"rotate by type width or more");
  c &= mask;
  return (n>>c) | (n<<( (-c)&mask ));
}

static inline uint16_t rotr16 (uint16_t n, unsigned int c)
{
  const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);

  // assert ( (c<=mask) &&"rotate by type width or more");
  c &= mask;
  return (n>>c) | (n<<( (-c)&mask ));
}

/* -- Functions -- */

char* hash_cust1(char* s, char* d, int n){
  int x = -1;
  while(s[++x]) d[x % n] = s[x] ^ (x < n ? '\0' : d[x % n] << 2);
  while(x < n) d[x++] = '\0';
  return d;
}

char* hash_cust2(char* s, char* d, int n){
  int x = -1;
  while(s[++x]) d[x % n] = s[x] ^ (s[x + 1] << 3);
  while(x < n) d[x++] = '\0';
  return d;
}

char* hash_cust3(char* s, char* d, int n){
  int x = -1;
  while(s[++x]){
    d[x % n] = s[x];
    d[x % sizeof(uint16_t)] ^= s[x] << 3;
  }
  while(x < n) d[x++] = '\0';
  return d;
}

/* NOTE: Currently used algorithm based dbj2. */
char* hash_cust4(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x) *(int16_t*)d = (*(int16_t*)d << 5) ^ *(int16_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Reduce running time and collisions */
char* hash_cust5(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x) *(int16_t*)d = (*(int16_t*)d << 3) ^ *(int16_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Experiment with 16 bit rotations */
char* hash_cust6(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x) *(int16_t*)d = rotl16(*(int16_t*)d, 5) ^ *(int16_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Experiment with larger integers */
char* hash_cust7(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x) *(int32_t*)d = (*(int32_t*)d << 3) ^ *(int32_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Decrease run time and collisions */
char* hash_cust8(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s;) *(int16_t*)d = (*(int16_t*)d << 5) ^ *(int16_t*)d + (d[++x % n] ^= *s++);
  return d;
}

/* Increase run time, improve 3-byte distribution */
char* hash_cust9(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s;){
    *(int16_t*)d = (*(int16_t*)d << 5) ^ *(int16_t*)d + (d[++x % n] ^= *s++);
    *(int16_t*)(d + (x % n - 1)) ^= *(int16_t*)d << 3;
  }
  return d;
}

/* sdbm gawk implementation from: http://www.cse.yorku.ca/~oz/hash.html */
char* hash_cust10(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x)
    *(int32_t*)d = (d[x % n] ^= *s++) + (*(int32_t*)d << 6) + (*(int32_t*)d << 16) - *(int32_t*)d;
  return d;
}

/* Decrease run time by removing minus, removing order dependence */
char* hash_cust11(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x)
    *(uint32_t*)d = (*(int32_t*)d << 16) + (*(int32_t*)d << 6) + *(int32_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Decrease run time, improve 3-byte distribution */
char* hash_cust12(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s; ++x)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x % n] ^= *s++);
  return d;
}

/* Inline computation */
char* hash_cust13(char* s, char* d, int n){
  memset(d, 0, n);
  for(int x = 0; *s;)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x++ % n] ^= *s++);
  return d;
}

/* Initial implementation of multi-byte hashing */
char* hash_cust14(char* s, char* d, int n){
  /* Copy s into d until either d filled or s length found */
  int l = -1;
  while(++l < n && s[l]) d[l] = s[l];
  /* If s too long, merge with existing bytes */
  while(s[l]) d[l % n] += s[l++];
  /* Finally do multi-byte merge over safe hash string */
  for(int x = 0; x < n;) d[x % n] = d[x % n] - (d[++x % n] << 3) + l;
  return d;
}

/* Full coverage, weak distribution, long run time */
char* hash_cust15(char* s, char* d, int n){
  /* Copy s into d until either d filled or s length found */
  int l = -1;
  while(++l < n && s[l]) d[l] = s[l];
  /* Back hash */
  for(int x = l; s[++x];) d[x % n] ^= s[x] + l;
  /* Forward hash */
  for(char* z = d; z < d + n - 1;)
    *(int16_t*)z = (*(int16_t*)z << 3) ^ *(int16_t*)z ^ (d[(int)(z - d) % 2] ^= *z++);
  return d;
}

/* Faster with terrible distribution */
char* hash_cust16(char* s, char* d, int n){
  memset(d, 0b10101010, n);
  /* Loop hash */
  int l = 0;
  while(*s) d[l % n] ^= *s++ ^ ++l;
  /* Forward hash */
  for(int x = 1; x < n; ++x) d[x] ^= (*d += ~d[d[x] % n] ^ l);
  return d;
}

/* Better wide distribution, better speed */
char* hash_cust17(char* s, char* d, int n){
  memset(d, 0b10101010, n);
  for(int x = 0; *s;)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x++ % n] ^= *s++);
  for(int x = 3; ++x < n;) d[x] = d[x] ^ d[x % 4] ^ d[x - 1];
  return d;
}

/* Much better distribution at the cost of computation speed */
char* hash_cust18(char* s, char* d, int n){
  memset(d, 0b10101010, n);
  for(int x = 0; *s;)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x++ % n] ^= *s++);
  for(int x = 3; ++x < n;)
    *(int16_t*)(d + x) = (*(int16_t*)(d + x) << 5) ^ *(int16_t*)(d + x) +
                         *(int16_t*)(d + (x % 4)) + *(int16_t*)(d + x - 1);
  return d;
}

/* Slightly better distribution */
char* hash_cust19(char* s, char* d, int n){
  memset(d, 0b10101010, n);
  for(int x = 0; *s;)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) + *(int32_t*)d + (d[x++ % n] ^= *s++);
  for(int x = 3; ++x < n - 1;)
    *(int16_t*)(d + x) = (*(int16_t*)(d + x) << 3) ^ *(int16_t*)(d + x) +
                         (*(int16_t*)(d + (x % 4)) ^ *(int16_t*)(d + x - 1));
  return d;
}

/* Reduction in running time, slight reduction in tail-end distribution */
char* hash_cust20(char* s, char* d, int n){
  memset(d, 0b10101010, n);
  int x = 0;
  while(*s)
    *(uint32_t*)d = *(int16_t*)(d + 2) + (*(int32_t*)d << 5) +
                    *(int32_t*)d       + (d[x++ % n] ^= *s++);
  for(; x < n - 1; ++x)
    *(int16_t*)(d + x) = (*(int16_t*)(d + x) << 3) ^ *(int16_t*)(d + x) +
                         (*(int16_t*)(d + (x % 4)) ^ *(int16_t*)(d + x - 1));
  return d;
}

/* -- Testing -- */

struct Result{
  char* (*func)(char*, char*, int);
  char* name;
  long int count;
  int64_t beg;
  int64_t end;
  long int collide8;
  long int collide16;
  long int collide24;
  long int collideL24;
  long int* seen8;
  long int* seen16;
  long int* seen24;
  long int* last24;
};

/* Source: https://stackoverflow.com/a/51336144/2847743 */
int64_t currentTimeMillis(){
  struct timeval time;
  gettimeofday(&time, NULL);
  int64_t s1 = (int64_t)(time.tv_sec) * 1000;
  int64_t s2 = time.tv_usec / 1000;
  return s1 + s2;
}

void test(struct Result* r, long int c){
  r->count = c;
  /* Setup memory */
  if(r->seen8 != NULL) free(r->seen8);
  r->seen8 = malloc((1 << (sizeof(uint8_t) * 8)) * sizeof(long int));
  memset(r->seen8, 0, sizeof(r->seen8) * sizeof(int));
  if(r->seen16 != NULL) free(r->seen16);
  r->seen16 = malloc((1 << (sizeof(uint16_t) * 8)) * sizeof(long int));
  memset(r->seen16, 0, sizeof(r->seen16) * sizeof(int));
  if(r->seen24 != NULL) free(r->seen24);
  r->seen24 = malloc((1 << ((sizeof(uint16_t) + sizeof(uint8_t)) * 8)) * sizeof(long int));
  memset(r->seen24, 0, sizeof(r->seen24) * sizeof(int));
  if(r->last24 != NULL) free(r->last24);
  r->last24 = malloc((1 << ((sizeof(uint16_t) + sizeof(uint8_t)) * 8)) * sizeof(long int));
  memset(r->last24, 0, sizeof(r->last24) * sizeof(int));
  char k[64];
  #define HASH_SIZE 128 / 8
  char h[HASH_SIZE];
  /* Start tests */
  r->beg = currentTimeMillis();
  for(long int x = 0; x < c; x++){
    sprintf(k, "%lx", x);
    r->func(k, h, 16);
    ++r->seen8[*(uint8_t*)h];
    ++r->seen16[*(uint16_t*)h];
    ++r->seen24[*(uint32_t*)h & 0xFFFFFF];
    ++r->last24[(*(uint32_t*)(h + (HASH_SIZE - 4)) >> 8) & 0xFFFFFF];
  }
  r->end = currentTimeMillis();
  /* Process collisions */
  r->collide8 = 0;
  for(long int x = 0; x < (1 << (sizeof(uint8_t) * 8)); x++){
    r->collide8 += r->seen8[x] <= 1 ? 0 : (r->seen8[x] - 1);
  }
  r->collide16 = 0;
  for(long int x = 0; x < (1 << (sizeof(uint16_t) * 8)); x++){
    r->collide16 += r->seen16[x] <= 1 ? 0 : (r->seen16[x] - 1);
  }
  r->collide24 = 0;
  for(long int x = 0; x < (1 << ((sizeof(uint16_t) + sizeof(uint8_t)) * 8)); x++){
    r->collide24 += r->seen24[x] <= 1 ? 0 : (r->seen24[x] - 1);
  }
  r->collideL24 = 0;
  for(long int x = 0; x < (1 << ((sizeof(uint16_t) + sizeof(uint8_t)) * 8)); x++){
    r->collideL24 += r->last24[x] <= 1 ? 0 : (r->last24[x] - 1);
  }
}

void display(struct Result* r){
  printf(
    "%s\t%li\t%li\t%li\t%li\t%li\n",
    r->name,
    r->end - r->beg,
    r->collide8,
    r->collide16,
    r->collide24,
    r->collideL24
  );
}

void save_img(struct Result* r, long int* seen, int m, char* part){
  #define G_MAX 255
  char filename[128];
  sprintf(filename, "%s_%s_%d.jpg", r->name, part, m);
  char* data = malloc(m * m);
  double min = DBL_MAX;
  double max = DBL_MIN;
  for(long int z = 0; z < (m * m); z++){
    min = seen[z] < min ? seen[z] : min;
    max = seen[z] > max ? seen[z] : max;
  }
  max -= min;
  max /= G_MAX;
  for(long int y = 0; y < m; y++){
    for(long int x = 0; x < m; x++){
      int v = (int)(G_MAX - ((seen[x + (m * y)] - min) / max));
      assert(v <= G_MAX);
      assert(v >= 0);
      data[x + (y * m)] = v;
    }
  }
  stbi_write_jpg(filename, m, m, 1, data, 80);
  free(data);
}

int main(){
  /* Setup test parameters */
  long int count = 256l * 256l * 256l;
  /* Setup memory  */
  #define NUM 20
  struct Result results[NUM] = {
    { .func =  hash_cust1, .name =  "cust1" },
    { .func =  hash_cust2, .name =  "cust2" },
    { .func =  hash_cust3, .name =  "cust3" },
    { .func =  hash_cust4, .name =  "cust4" },
    { .func =  hash_cust5, .name =  "cust5" },
    { .func =  hash_cust6, .name =  "cust6" },
    { .func =  hash_cust7, .name =  "cust7" },
    { .func =  hash_cust8, .name =  "cust8" },
    { .func =  hash_cust9, .name =  "cust9" },
    { .func = hash_cust10, .name = "cust10" },
    { .func = hash_cust11, .name = "cust11" },
    { .func = hash_cust12, .name = "cust12" },
    { .func = hash_cust13, .name = "cust13" },
    { .func = hash_cust14, .name = "cust14" },
    { .func = hash_cust15, .name = "cust15" },
    { .func = hash_cust16, .name = "cust16" },
    { .func = hash_cust17, .name = "cust17" },
    { .func = hash_cust18, .name = "cust18" },
    { .func = hash_cust19, .name = "cust19" },
    { .func = hash_cust20, .name = "cust20" },
  };
  /* NULL values */
  for(int x = 0; x < NUM; x++){
    results[x].seen8 = NULL;
    results[x].seen16 = NULL;
    results[x].seen24 = NULL;
    results[x].last24 = NULL;
  }
  /* Perform tests */
  for(int x = 0; x < NUM; x++) test(&results[x], count);
  /* Print results */
  printf("func\tms\tcol=8\tcol=16\tcol=24\n");
  for(int x = 0; x < NUM; x++) display(&results[x]);
  /* Visualize results */
  for(int x = 0; x < NUM; x++){
    save_img(&results[x], results[x].seen8,    16, "init");
    save_img(&results[x], results[x].seen16,  256, "init");
    save_img(&results[x], results[x].seen24, 4096, "init");
    save_img(&results[x], results[x].last24, 4096, "last");
  }
}
